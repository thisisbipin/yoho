" -------------------- Plugins ----------------------------
call plug#begin('~/.vim/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'christoomey/vim-tmux-navigator'
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'scrooloose/nerdcommenter'
Plug 'morhetz/gruvbox'
call plug#end()





" -------------------- Mapping for Plugins ----------------------------
nmap ++ <plug>NERDCommenterToggle
vmap ++ <plug>NERDCommenterToggle


" ---------- For the side file explorer ---------------------
nnoremap <C-b> :NERDTree<CR>
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }



" ---------- some default customizations ---------------------
set nocompatible
set nu rnu
"syntax on
set mouse=a mousemodel=popup
map <ScrollWheelUp> <C-Y>
map <ScrollWheelDown> <C-E>
set autoindent
set tabstop=4
set ruler
set smarttab
set linebreak
set spell
set et
set title
set clipboard=unnamed
" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" No sound on error
set noerrorbells
set novisualbell
"set t_vb=
set tm=500


set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines


" --------- Color scheme ------
color gruvbox
